# dxsolutions-test #

CRUD application by Michiek Vanhaverbeke as test studetenjob DX-solutions


## assingment ##
### 
Test deadline 02/06  : ###

Create a CRUD system create, read, update, delete.

What is the CRUD about: Jobs + location

Technical:

Framework Symfony2/3 or **Laravel 5**

Query: use an ORM, ... do not use plain sql code

Front: Twitter bootstrap

API: items should be available via an REST API (oAuth2 secured).

Dependancies: Composer

Versioning: GIT using bitbucket name dxsolutions-test

## installation ##

* clone repo
* place crud folder into htdocs
* use jobs.sql to create the db
* application available under localhost/crud/public/crud
* api available under localhost/crud/public/api
* single item api available under localhost/crud/public/crud/?id


### happy testing and i will hopefully see you this summer ###

if you have any questions or remarks please mail michielvanhaverbekewaregem@gmail.com