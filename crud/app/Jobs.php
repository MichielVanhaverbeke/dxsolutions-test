<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobs extends Model
{
    protected $fillable = [
        'jobname',
        'jobdesc',
        'joblocation'
    ];
    public $timestamps = false;
    protected $table = 'jobs';
    Protected $primaryKey = "ID";
}
