<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class pagesController extends Controller
{
    public function getCreate(){

        $name = 'create';
        return view("pages.create")->with("name", $name);

    }

    public function getRead(){
        $name = 'read';
        return view("pages.read")
            ->with("name", $name);
    }

    public function getUpdate(){
        $name = 'update';
        return view("pages.update")
            ->with("name", $name);
    }

    public function getDelete(){
        $name = 'delete';
        return view("pages.delete")
            ->with("name", $name);
    }
}