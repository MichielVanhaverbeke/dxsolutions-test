<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\jobs;
use App\Http\Requests;

class apiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = jobs::all();
        return($jobs);
    }



    public function show($id)
    {
        $job = jobs::find($id);

        return($job);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



}
