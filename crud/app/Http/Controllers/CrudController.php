<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\jobs;
use App\Http\Requests;

class CrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = jobs::all();
        $name = 'display';

        return view('pages.index')
            ->with('jobs', $jobs)
            ->with('name', $name);
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $job = new jobs;
        $job->jobname = $request->name;
        $job->jobdesc = $request->description;
        $job->joblocation = $request->location;
        $job->save();

        return redirect()->route('crud.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $job = jobs::find($id);
        $name = 'read';
        return view('pages.read')
            ->with('job', $job)
            ->with('name', $name);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $job = jobs::find($id);
        $name = 'update';
        return view('pages.update')
            ->with('job', $job)
            ->with('name', $name);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $job = jobs::find($id);

        $job->jobname = $request->name;
        $job->jobdesc = $request->description;
        $job->joblocation = $request->location;
        $job->save();

        return redirect()->route('crud.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $job =  jobs::where('ID', $id)->delete();
        return redirect()->route('crud.index');
    }
}
