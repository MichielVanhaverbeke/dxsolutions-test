<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>

    {!! HTML::style('css/bootstrap.min.css') !!}
    {!! HTML::style('css/style.css') !!}
</head>
<body>
    @include('layouts.menu')
    <section class="main">
    @yield('body')
    </section>
    {!! HTML::script('js/jquery-1.12.4.js') !!}
    {!! HTML::script('js/bootstrap.min.js') !!}
</body>
</html>