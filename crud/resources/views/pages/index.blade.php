@extends('layouts.layout')
@section('title')
    {{$name}}

@stop


@section('body')
    <section class="title">
    <h2>this is the {{$name}} page</h2>
    </section>

    <table class="table">
        <tr>
            <th>id</th>
            <th>jobname</th>
            <th>jobdescription</th>
            <th>joblocation</th>
            <th></th>
        </tr>
    @foreach ($jobs as $job)
        <tr>
            <td>{{$job->ID}}</td>
        <td>{{ $job->jobname }}</td>
        <td>{{ $job->jobdesc }}</td>
            <td>{{ $job->joblocation }}</td>
        <td><a href="{{route('crud.show', $job->ID)}}" class="btn btn-primary">select</a></td>
        </tr>
    @endforeach
    </table>
    <a href="{{route('crud.create')}}" class="btn btn-success">new entry</a>
@stop