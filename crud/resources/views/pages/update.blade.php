@extends('layouts.layout')
@section('title')
    {{$name}}
@stop

@section('body')


    <section class="title">
    <h2>you have chosen to edit record with id: {{$job->ID}}</h2>
    </section>
    <div id="form">
    {!!Form::model($job,[
    'method' => 'patch',
    'route' => ['crud.update', $job->ID]])!!}

    {!! Form::label('name', 'name') !!}
    {!! Form::text('name', $job->jobname,['placeholder' => 'name', 'class' => 'form-control']) !!}<br>

    {!! Form::label('description', 'description') !!}
    {!! Form::text('description', $job->jobdesc,['placeholder' => 'desc', 'class' => 'form-control']) !!}<br>

    {!! Form::label('location', 'location') !!}
    {!! Form::text('location', $job->joblocation ,['placeholder' => 'location', 'class' => 'form-control']) !!}<br>

    {!! Form::submit('save', ['class'=>'btn btn-primary'])!!}
    {!! Form::close()!!}
    </div>
@stop