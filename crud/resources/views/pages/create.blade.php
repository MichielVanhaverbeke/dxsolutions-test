@extends('layouts.layout')
    @section('title')
        create
    @stop

    @section('body')
        <section class="title">
            <h2>create a new record</h2>
        </section>

        <div id="form">

        {!!Form::open(['route' => 'crud.store'])!!}

        {!! Form::label('name', 'name') !!}
        {!! Form::text('name', null,['placeholder' => 'name', 'class' => 'form-control']) !!}<br>

        {!! Form::label('description', 'description') !!}
        {!! Form::text('description', null,['placeholder' => 'desc', 'class' => 'form-control']) !!}<br>

        {!! Form::label('location', 'location') !!}
        {!! Form::text('location', null,['placeholder' => 'location', 'class' => 'form-control']) !!}<br>

        {!! Form::submit('create',['class' => 'btn btn-primary']) !!}

        {!! Form::close() !!}
        </div>
    @stop


