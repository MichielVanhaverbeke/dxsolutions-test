@extends('layouts.layout')
@section('title')
    {{$name}}
@stop

@section('body')

    <section class="title">
    <h2>you have selected the item with id: {{$job->ID}}</h2>
    </section>

    {!!Form::open([
    'method'=> 'delete',
    'route' => ['crud.destroy', $job->ID]])!!}
    <table class="table">
        <tr>
            <th>id</th>
            <th>jobname</th>
            <th>jobdescription</th>
            <th>joblocation</th>

        </tr>
        <tr>
        <td>{{$job->ID}}</td>
        <td>{{ $job->jobname }}</td>
        <td>{{ $job->jobdesc }}</td>
        <td>{{ $job->joblocation }}</td>
        </tr>
    </table>





    <a href="{{route('crud.edit', $job->ID)}}" class="btn btn-primary">edit</a>
    {!! Form::submit('delete', ['class'=>'btn btn-danger'])!!}
    {!! Form::close()!!}
@stop